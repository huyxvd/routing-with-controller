using Microsoft.AspNetCore.Mvc;

WebApplicationBuilder builder = WebApplication.CreateBuilder();

builder.Services.AddControllers();

WebApplication app = builder.Build();

app.MapControllers();

app.Run();

[ApiController]
[Route("[controller]")]
public class BooksController : ControllerBase
{
    private static List<Book> books = new List<Book>();

    // GET /books
    [HttpGet]
    public List<Book> GetBooks() => books;

    // GET /books/123
    [HttpGet("{id}")]
    public ActionResult<Book> GetBookById(int id)
    {
        Book book = books.FirstOrDefault(b => b.Id == id);
        if (book is null)
        {
            return NotFound();
        }
        return book;
    }

    // POST /books
    [HttpPost]
    public ActionResult<Book> CreateBook(Book book)
    {
        int newId = books.Count + 1;
        book.Id = newId;
        books.Add(book);
        return Ok();
    }

    // PUT /books/123
    [HttpPut("{id}")]
    public IActionResult UpdateBook(int id, Book book)
    {
        Book item = books.FirstOrDefault(item => item.Id == id);
        if (item is null)
        {
            return NotFound();
        }
        item.Title = book.Title;
        item.Author = book.Author;
        return Ok(item);
    }

    // DELETE /books/123
    [HttpDelete("{id}")]
    public IActionResult DeleteBook(int id)
    {
        var bookToRemove = books.FirstOrDefault(b => b.Id == id);
        if (bookToRemove is null)
        {
            return NotFound();
        }
        books.Remove(bookToRemove);
        return NoContent();
    }
}

public class Book
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string Author { get; set; }
}